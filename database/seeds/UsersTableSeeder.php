<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'      => 'Abiezer Sifontes',
            'email'     => 'asifontes@cabelum.com.ve',
            'password'  =>  bcrypt('2580abiezer'),
            'role'      =>  'admin'
        ]);
        DB::table('users')->insert([
            'name'      => 'Karla Castro',
            'email'     => 'kcastro@cabelum.com.ve',
            'password'  =>  bcrypt('kcastro'),
            'role'      =>  'editor'
        ]);
    }
}
